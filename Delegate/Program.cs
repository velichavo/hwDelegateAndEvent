﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Delegate
{
    class Program
    {
        static void Main(string[] args)
        {
            List<MyType> myList = new List<MyType>();
            for (int i = 0; i <= 10; i++)
            {
                myList.Add(new MyType());
                Console.WriteLine(myList.ElementAt(i).MyElement);
            }
            Console.WriteLine();

            MyType type = myList.GetMax(MyType.func);
            Console.WriteLine("Max element = " + type.MyElement);



        }
    }
    public static class ListExtention
    {
        public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParameter) where T : class
        {
            T element = e.First();
            for (int i = 1; i < e.Count(); i++)
            {
                if(getParameter(e.ElementAt(i)) > getParameter(element))
                {
                    element = e.ElementAt(i);
                }
            }

            return element;
        }
    }
    public class MyType
    {
        readonly Random rnd;
        public float MyElement;
        public MyType()
        {
            rnd = new Random();
            MyElement = (float)rnd.NextDouble() * 100;
        }

        public static Func<MyType, float> func = mt => mt.MyElement;
    }
}
