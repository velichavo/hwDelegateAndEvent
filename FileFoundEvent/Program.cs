﻿using System;
using System.IO;
using System.Threading;

namespace FileFoundEvent
{
    class Program
    {
        public static void Main(string[] args)
        {
            string path = Directory.GetCurrentDirectory();

            if (Directory.Exists(path))
            {

                FindFile findFile = new FindFile();
                findFile.OnFoundFile += FoundFile;
                findFile.OnCanceledButton += PushButton;
                findFile.ProcessDirectory(path);
            }
            else
            {
                Console.WriteLine("{0} is not a valid file or directory.", path);
            }

            Console.WriteLine("END");
            Console.ReadKey();
        }


        //обработчик события
        protected static void FoundFile(object sender, FileEventArgs e)
        {
            ProcessFile(e.FileName);
        }
        public static void ProcessFile(string path)
        {
            Console.WriteLine("Processed file '{0}'.", path);
        }

        protected static bool PushButton(CanceledButton button)
        {
            button.WaitPushKey();
            bool cancel = (button.CanceledKey == button.CurrentKey);
            return cancel;       
        }



    }

    public class CanceledButton
    {
        ConsoleKey canceledKey = ConsoleKey.Spacebar;
        ConsoleKey currentKey;

        public ConsoleKey CanceledKey { get => canceledKey; set => canceledKey = value; }
        public ConsoleKey CurrentKey { get => currentKey; set => currentKey = value; }

        public void WaitPushKey()
        {
            Console.WriteLine("PRESSED KEY SPACEBAR TO STOP");
            CurrentKey = Console.ReadKey(true).Key;
        }
    }
}
