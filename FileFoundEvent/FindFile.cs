﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading;

namespace FileFoundEvent
{
    class FindFile
    {
        public event EventHandler<FileEventArgs> OnFoundFile;
        public event Predicate<CanceledButton> OnCanceledButton;
        bool canceled = false;
        readonly CanceledButton button;
        private readonly Thread th;

        public FindFile()
        {
            button = new CanceledButton();
            ThreadStart start = IsCanceled;
            th = new Thread(start);
            th.Start();
        }
        public void ProcessDirectory(string targetDirectory)
        {        
            string[] fileEntries = Directory.GetFiles(targetDirectory);
            foreach (string fileName in fileEntries)
            {
                if (canceled) break;
                OnFoundFile?.Invoke(this, new FileEventArgs(fileName));
                Thread.Sleep(500);
            }

            //Поиск в поддиректориях.
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
            foreach (string subdirectory in subdirectoryEntries)
            {
                if (canceled) break;
                ProcessDirectory(subdirectory);
            }
        }

        protected void IsCanceled()
        {
            while (true)
                if (OnCanceledButton != null && !canceled)
                    if (OnCanceledButton.Invoke(button))
                    {
                        canceled = true;
                        break;
                    }
        }
    }
}
