﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileFoundEvent
{
    class FileEventArgs : EventArgs
    {
        string fileName;

        public string FileName { get => fileName; set => fileName = value; }

        public FileEventArgs(string fileName)
        {
            this.FileName = fileName;
        }

    }
}
